const express = require("express");


// Mongoose is a package that allows creating of Schemas to our model our data structures

// Also has a access to a number of methods of manipulating our database
const mongoose = require("mongoose")

const app = express();
const port = 3001;

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://gioangelofelix2:admin123@zuitt-bootcamp.gwsbg1x.mongodb.net/s35?retryWrites=true&w=majority",
	{
		// allows us to avoid any currrent and future errors while connecting to MongoDB
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Allows to handle errors when the initial connection is established 
let db = mongoose.connection;

// console.error.bind(console) allows to print errors in browser console and in terminal.
// "connection error" ois the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

// Connecting to MongoDB atlas end
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// [SECTION] Mongoose schema
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});
// [SECTION] Mongoose Models
// Models must be in singular form and capitalized
const Task = mongoose.model("Task", taskSchema);

// Creation of Task App Application
// Create a new task
/*
BUSINESS LOGIC
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return an error
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body (postman)
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req, res) => {
		Task.findOne({name: req.body.name}).then((result, err) => {
			if(result != null && result.name == req.body.name) {
				return res.send("Duplicate task found");
			} else {
				let newTask = new Task({
					name: req.body.name
				});
				newTask.save().then((savedTask, saveErr) => {
					if(saveErr){
						return console.error(saveErr)
					} else {
						return res.status(201).send("New task created!");
					}
				})
			}
		})
});

// Getting all the tasks
/*
BUSINESS LOGIC
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/
app.get("/tasks", (req, res) => {
	Task.find({}).then((result, err) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
});

// [SECTION] - S35 ACTIVITY 
// Mongoose Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});
// Mongoose Model
const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
		User.find({username: req.body.username}, {password: req.body.username}).then((result, err) => {
			if(result != null && result.username == req.body.username && result.password == req.body.password) {
				return res.send("Duplicate user found");
			} else {
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				});
				newUser.save().then((savedTask, saveErr) => {
					if(saveErr){
						return console.error(saveErr)
					} else {
						return res.status(201).send("New User created!");
					}
				})
			}
		})
});

app.get("/signup", (req, res) => {
	User.find({}).then((result, err) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
});




app.listen(port, () => console.log(`Server running at port ${port}`));




